/*
 Navicat Premium Data Transfer

 Source Server         : chenrds
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : 39.103.10.7:3306
 Source Schema         : yun

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 12/07/2020 01:01:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for file
-- ----------------------------
DROP TABLE IF EXISTS `file`;
CREATE TABLE `file`  (
  `fileId` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `filePath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`fileId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of file
-- ----------------------------
INSERT INTO `file` VALUES (1, '1234', '\\\\b');
INSERT INTO `file` VALUES (2, '1234', '\\\\a');
INSERT INTO `file` VALUES (4, 'lll1', '\\\\2020陈宏阳.pdf');
INSERT INTO `file` VALUES (5, 'chen1', '\\\\2020陈宏阳.pdf');
INSERT INTO `file` VALUES (6, 'prchen1', '\\\\image\\LLLLinux.docx');
INSERT INTO `file` VALUES (7, 'prchen1', '\\\\82542848_p0.png');

-- ----------------------------
-- Table structure for office
-- ----------------------------
DROP TABLE IF EXISTS `office`;
CREATE TABLE `office`  (
  `officeid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `officeMd5` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`officeMd5`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of office
-- ----------------------------
INSERT INTO `office` VALUES ('124', '124');
INSERT INTO `office` VALUES ('doc-hi2m3psn08i4smn', '3AC0DD267D32943CF839077793B7CEB5');
INSERT INTO `office` VALUES ('doc-hjkmw0rhhqj1b9a', 'C9F575B30E794F8A10DE898ACFFC906A');
INSERT INTO `office` VALUES ('doc-hjar6mysr2et8ep', 'E82BC0449F096AD446B20F2516DAE912');

-- ----------------------------
-- Table structure for share
-- ----------------------------
DROP TABLE IF EXISTS `share`;
CREATE TABLE `share`  (
  `shareId` int(11) NOT NULL AUTO_INCREMENT,
  `shareUrl` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `shareUser` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '1公开 2加密 -1已取消',
  `command` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '提取码',
  PRIMARY KEY (`shareId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of share
-- ----------------------------
INSERT INTO `share` VALUES (1, '', '\\\\image', '123', 1, NULL);
INSERT INTO `share` VALUES (2, '3c7c5ebf', '\\\\image\\18505720_144032556135_2.jpg', '1234', -1, NULL);
INSERT INTO `share` VALUES (3, 'af2ec3df', '\\\\image\\18505720_144032556135_2.jpg', '1234', -1, NULL);
INSERT INTO `share` VALUES (4, '066eb56c', '\\\\image\\18505720_144032556135_2.jpg', '1234', -1, NULL);
INSERT INTO `share` VALUES (5, '2666980e', '\\\\image\\18505720_144032556135_2.jpg', '1234', -1, NULL);
INSERT INTO `share` VALUES (6, 'd55d05db', '\\\\image\\18505720_144032556135_2.jpg', '1234', -1, NULL);
INSERT INTO `share` VALUES (7, 'd4707693', '\\\\image\\18505720_144032556135_2.jpg', '1234', -1, NULL);
INSERT INTO `share` VALUES (8, 'c7719377', '\\\\image\\18505720_144032556135_2.jpg', '1234', -1, NULL);
INSERT INTO `share` VALUES (9, 'f2254581', '\\\\image\\Q (6).png', '1234', 1, NULL);
INSERT INTO `share` VALUES (10, 'b871716b', '\\\\ b\\Q1.png', '1234', 1, NULL);
INSERT INTO `share` VALUES (11, '8b005a74', '\\\\ b\\Q1.png', '1234', 1, NULL);
INSERT INTO `share` VALUES (12, '0ce7a66a', '\\\\ b\\Q1.png', '1234', 1, NULL);
INSERT INTO `share` VALUES (13, 'a8713e6c', '\\\\ b\\Q1.png', '1234', 1, NULL);
INSERT INTO `share` VALUES (14, '7a954670', '\\\\ b\\Q1.png', '1234', 1, NULL);
INSERT INTO `share` VALUES (15, '66e9b420', '\\\\ b\\Q (6).png', '1234', 1, NULL);
INSERT INTO `share` VALUES (16, 'c8cb7c0d', '\\\\视频题目.docx', 'ccc11', 1, NULL);
INSERT INTO `share` VALUES (17, '4746fe73', '\\\\image\\Hyper Scape2020-7-3-18-17-10.jpg', 'q12', 0, NULL);
INSERT INTO `share` VALUES (18, 'ac11894f', '\\\\music\\新建文本文档.txt', 'q12', 0, NULL);
INSERT INTO `share` VALUES (19, 'a4fa3aee', '\\\\music\\新建文本文档.txt', 'q12', 1, NULL);
INSERT INTO `share` VALUES (20, '1e8503e6', '\\\\jdk api 1.8.CHM', 'q12', 1, NULL);
INSERT INTO `share` VALUES (21, '6d99b6f0', '\\\\image\\Hyper Scape2020-7-3-18-17-10.jpg', 'q12', 1, NULL);
INSERT INTO `share` VALUES (22, 'b26d26fd', '\\\\image\\Hyper Scape2020-7-3-18-17-10.jpg', 'q12', 1, NULL);
INSERT INTO `share` VALUES (23, '154c863f', '\\\\cxvd', 'q12', 1, NULL);
INSERT INTO `share` VALUES (24, '20d93823', '\\\\2020陈宏阳.pdf', 'lll1', 1, NULL);
INSERT INTO `share` VALUES (25, 'd984ac10', '\\\\2020陈宏阳.pdf', 'lll1', 1, NULL);

-- ----------------------------
-- Table structure for system_log
-- ----------------------------
DROP TABLE IF EXISTS `system_log`;
CREATE TABLE `system_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `des` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记载登录 还是注册',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 130 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_log
-- ----------------------------
INSERT INTO `system_log` VALUES (35, '2020-07-08 20:43:06', 'q12', '登录');
INSERT INTO `system_log` VALUES (36, '2020-07-08 20:56:40', 'q12', '登录');
INSERT INTO `system_log` VALUES (37, '2020-07-08 21:00:01', 'q12', '登录');
INSERT INTO `system_log` VALUES (38, '2020-07-08 21:01:47', 'q12', '登录');
INSERT INTO `system_log` VALUES (39, '2020-07-08 21:29:41', 'q12', '登录');
INSERT INTO `system_log` VALUES (40, '2020-07-08 21:34:48', 'q12', '登录');
INSERT INTO `system_log` VALUES (41, '2020-07-08 21:35:34', 'q12', '登录');
INSERT INTO `system_log` VALUES (42, '2020-07-08 21:42:24', 'q12', '登录');
INSERT INTO `system_log` VALUES (43, '2020-07-08 21:42:32', 'q12', '登录');
INSERT INTO `system_log` VALUES (44, '2020-07-08 21:43:26', 'q12', '登录');
INSERT INTO `system_log` VALUES (45, '2020-07-08 21:43:35', 'q12', '登录');
INSERT INTO `system_log` VALUES (46, '2020-07-08 21:45:15', 'q12', '登录');
INSERT INTO `system_log` VALUES (47, '2020-07-08 21:47:42', 'q12', '登录');
INSERT INTO `system_log` VALUES (48, '2020-07-08 21:48:15', 'q12', '登录');
INSERT INTO `system_log` VALUES (49, '2020-07-08 21:50:42', 'q12', '登录');
INSERT INTO `system_log` VALUES (50, '2020-07-08 21:50:50', 'q12', '登录');
INSERT INTO `system_log` VALUES (51, '2020-07-08 21:51:56', 'q12', '登录');
INSERT INTO `system_log` VALUES (52, '2020-07-08 21:52:42', 'q12', '登录');
INSERT INTO `system_log` VALUES (53, '2020-07-08 21:53:48', 'q12', '登录');
INSERT INTO `system_log` VALUES (54, '2020-07-08 21:56:36', 'q12', '登录');
INSERT INTO `system_log` VALUES (55, '2020-07-08 22:12:54', 'q12', '登录');
INSERT INTO `system_log` VALUES (56, '2020-07-08 22:14:31', 'q12', '登录');
INSERT INTO `system_log` VALUES (57, '2020-07-08 22:18:16', 'q12', '登录');
INSERT INTO `system_log` VALUES (58, '2020-07-08 22:19:27', 'q12', '登录');
INSERT INTO `system_log` VALUES (59, '2020-07-08 22:20:57', 'q12', '登录');
INSERT INTO `system_log` VALUES (60, '2020-07-08 22:22:04', 'q12', '登录');
INSERT INTO `system_log` VALUES (61, '2020-07-08 22:23:25', 'q12', '登录');
INSERT INTO `system_log` VALUES (62, '2020-07-08 22:23:57', 'q12', '登录');
INSERT INTO `system_log` VALUES (63, '2020-07-08 22:25:10', 'q12', '登录');
INSERT INTO `system_log` VALUES (64, '2020-07-08 22:27:42', 'q12', '登录');
INSERT INTO `system_log` VALUES (65, '2020-07-08 22:29:30', 'q12', '登录');
INSERT INTO `system_log` VALUES (66, '2020-07-08 22:40:07', 'q12', '登录');
INSERT INTO `system_log` VALUES (67, '2020-07-08 23:09:04', 'lll1', '注册');
INSERT INTO `system_log` VALUES (68, '2020-07-08 23:11:34', 'lll1', '登录');
INSERT INTO `system_log` VALUES (69, '2020-07-09 00:24:08', 'lll1', '登录');
INSERT INTO `system_log` VALUES (70, '2020-07-09 00:38:00', 'lll1', '登录');
INSERT INTO `system_log` VALUES (71, '2020-07-09 12:42:54', 'lll1', '登录');
INSERT INTO `system_log` VALUES (72, '2020-07-09 13:28:58', 'lll1', '登录');
INSERT INTO `system_log` VALUES (73, '2020-07-09 13:29:18', 'chen1', '注册');
INSERT INTO `system_log` VALUES (74, '2020-07-09 13:29:25', 'chen1', '登录');
INSERT INTO `system_log` VALUES (75, '2020-07-09 14:06:14', 'chen1', '登录');
INSERT INTO `system_log` VALUES (76, '2020-07-09 14:36:34', 'chen1', '登录');
INSERT INTO `system_log` VALUES (77, '2020-07-09 14:38:45', 'chen1', '登录');
INSERT INTO `system_log` VALUES (78, '2020-07-09 14:40:31', 'chen1', '登录');
INSERT INTO `system_log` VALUES (79, '2020-07-09 14:41:55', 'chen1', '登录');
INSERT INTO `system_log` VALUES (80, '2020-07-09 14:51:45', 'chen1', '登录');
INSERT INTO `system_log` VALUES (81, '2020-07-09 14:53:27', 'chen1', '登录');
INSERT INTO `system_log` VALUES (82, '2020-07-09 14:56:33', 'chen1', '登录');
INSERT INTO `system_log` VALUES (83, '2020-07-09 15:00:18', 'chen1', '登录');
INSERT INTO `system_log` VALUES (84, '2020-07-09 15:02:49', 'chen1', '登录');
INSERT INTO `system_log` VALUES (85, '2020-07-09 15:05:32', 'chen1', '登录');
INSERT INTO `system_log` VALUES (86, '2020-07-09 15:09:30', 'chen1', '登录');
INSERT INTO `system_log` VALUES (87, '2020-07-09 15:11:03', 'chen1', '登录');
INSERT INTO `system_log` VALUES (88, '2020-07-09 15:12:49', 'chen1', '登录');
INSERT INTO `system_log` VALUES (89, '2020-07-09 15:16:32', 'chen1', '登录');
INSERT INTO `system_log` VALUES (90, '2020-07-09 15:18:38', 'chen1', '登录');
INSERT INTO `system_log` VALUES (91, '2020-07-09 15:22:59', 'chen1', '登录');
INSERT INTO `system_log` VALUES (92, '2020-07-09 15:29:51', 'chen1', '登录');
INSERT INTO `system_log` VALUES (93, '2020-07-09 15:32:20', 'chen1', '登录');
INSERT INTO `system_log` VALUES (94, '2020-07-09 15:34:23', 'chen1', '登录');
INSERT INTO `system_log` VALUES (95, '2020-07-09 15:36:51', 'chen1', '登录');
INSERT INTO `system_log` VALUES (96, '2020-07-09 15:39:14', 'chen1', '登录');
INSERT INTO `system_log` VALUES (97, '2020-07-09 15:55:36', 'chen1', '登录');
INSERT INTO `system_log` VALUES (98, '2020-07-09 16:15:56', 'chen1', '登录');
INSERT INTO `system_log` VALUES (99, '2020-07-09 16:19:01', 'prchen1', '注册');
INSERT INTO `system_log` VALUES (100, '2020-07-09 16:19:08', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (101, '2020-07-09 16:21:42', 'chen1', '登录');
INSERT INTO `system_log` VALUES (102, '2020-07-09 16:22:01', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (103, '2020-07-09 16:26:09', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (104, '2020-07-09 16:28:14', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (105, '2020-07-09 16:36:39', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (106, '2020-07-09 16:37:20', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (107, '2020-07-09 16:43:03', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (108, '2020-07-09 16:46:15', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (109, '2020-07-09 16:48:41', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (110, '2020-07-09 16:58:31', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (111, '2020-07-09 17:00:47', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (112, '2020-07-09 17:02:40', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (113, '2020-07-09 17:04:45', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (114, '2020-07-09 17:38:06', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (115, '2020-07-09 17:39:27', 'prq1', '登录');
INSERT INTO `system_log` VALUES (116, '2020-07-09 17:45:14', 'prq1', '登录');
INSERT INTO `system_log` VALUES (117, '2020-07-09 17:47:16', 'prq1', '登录');
INSERT INTO `system_log` VALUES (118, '2020-07-09 17:51:05', 'prq1', '登录');
INSERT INTO `system_log` VALUES (119, '2020-07-10 01:22:24', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (120, '2020-07-10 02:06:46', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (121, '2020-07-10 02:08:30', 'zzz1234566', '注册');
INSERT INTO `system_log` VALUES (122, '2020-07-10 02:08:57', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (123, '2020-07-10 02:30:22', 'hgfas', '注册');
INSERT INTO `system_log` VALUES (124, '2020-07-10 19:12:58', 'cvncvn1231', '注册');
INSERT INTO `system_log` VALUES (125, '2020-07-10 19:13:26', 'prchen1', '登录');
INSERT INTO `system_log` VALUES (126, '2020-07-10 19:15:55', 'mnbc12345', '注册');
INSERT INTO `system_log` VALUES (127, '2020-07-10 19:18:25', 'mnbc12345', '登录');
INSERT INTO `system_log` VALUES (128, '2020-07-10 19:18:25', 'mnbc12345', '登录');
INSERT INTO `system_log` VALUES (129, '2020-07-11 21:59:14', 'hgfas', '登录');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'userID同时也是faceid',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `countSize` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0.0B',
  `totalSize` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '10.0GB',
  `vip` int(3) NULL DEFAULT NULL COMMENT 'vip状态',
  `privateKey` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '私密空间密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'prchen1', 'DC483E80A7A0BD9EF71D8CF973673924', '2.2MB', '10.0GB', 0, '1');
INSERT INTO `user` VALUES (100, 'prq1', 'DC483E80A7A0BD9EF71D8CF973673924', '0B', '10.0GB', 0, '1');
INSERT INTO `user` VALUES (101, 'pp11', 'AF8F9DFFA5D420FBC249141645B962EE', '0.0B', '10.0GB', 0, NULL);
INSERT INTO `user` VALUES (102, 'a1234', 'AF8F9DFFA5D420FBC249141645B962EE', '0.0B', '10.0GB', 0, '123');
INSERT INTO `user` VALUES (103, 'zzz1234566', 'AF8F9DFFA5D420FBC249141645B962EE', '0.0B', '10.0GB', 0, '123');
INSERT INTO `user` VALUES (104, 'hgfas', 'AF8F9DFFA5D420FBC249141645B962EE', '0.0B', '10.0GB', 0, '123');
INSERT INTO `user` VALUES (105, 'cvncvn1231', 'AF8F9DFFA5D420FBC249141645B962EE', '0.0B', '10.0GB', 0, '123');
INSERT INTO `user` VALUES (106, 'mnbc12345', 'AF8F9DFFA5D420FBC249141645B962EE', '0.0B', '10.0GB', 0, '123');

-- ----------------------------
-- Table structure for vip
-- ----------------------------
DROP TABLE IF EXISTS `vip`;
CREATE TABLE `vip`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of vip
-- ----------------------------
INSERT INTO `vip` VALUES (1, '1', 0);
INSERT INTO `vip` VALUES (2, '2', 1);
INSERT INTO `vip` VALUES (3, '3', 1);
INSERT INTO `vip` VALUES (4, '4', 1);

SET FOREIGN_KEY_CHECKS = 1;
