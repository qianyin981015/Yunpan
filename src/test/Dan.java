package test;

/**
 * @author 陈宏阳
 * @date 2020/7/13
 * Yunpan
 */
public class Dan {
    public Dan(){};
    private static volatile Dan dan;
    public static Dan getInstance(){
        if(dan==null){
            synchronized (Dan.class){
            if(dan==null){
                dan=new Dan();
                }
            }
        }
        return dan;
    }
}
