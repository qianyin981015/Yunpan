package test.abstractFactory;

/**
 * @author 陈宏阳
 * @date 2020/7/13
 * Yunpan
 */
public class mi implements abstractFactory{
    @Override
    public PC makePc() {
        return new miPC();
    }

    @Override
    public Phone makePhone() {
        return new miPhone();
    }
}
