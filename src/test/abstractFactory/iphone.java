package test.abstractFactory;

/**
 * @author 陈宏阳
 * @date 2020/7/13
 * Yunpan
 */
public class iphone implements Phone{
    @Override
    public void make() {
    System.out.println("make iphone");
    }
    public iphone(){
        this.make();
    }
}
