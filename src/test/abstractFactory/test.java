package test.abstractFactory;

/**
 * @author 陈宏阳
 * @date 2020/7/13
 * Yunpan
 */
public class test {
    public static void main(String[] args){
        abstractFactory miFactory=new mi();
        abstractFactory iphoneFactory=new apple();
        miFactory.makePc();
        miFactory.makePhone();
        iphoneFactory.makePc();
        iphoneFactory.makePhone();
    }
}
