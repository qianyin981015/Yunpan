package test.abstractFactory;

/**
 * @author 陈宏阳
 * @date 2020/7/13
 * Yunpan
 */
public class miPhone implements Phone{
    @Override
    public void make() {
    System.out.println("make miPhone");
    }
    public miPhone(){
        this.make();
    }
}
