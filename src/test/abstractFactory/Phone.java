package test.abstractFactory;

/**
 * @author 陈宏阳
 * @date 2020/7/13
 * Yunpan
 */
public interface Phone {
    void make();
}
