package test.abstractFactory;

/**
 * @author 陈宏阳
 * @date 2020/7/13
 * Yunpan
 */
public class apple implements abstractFactory{

    @Override
    public PC makePc() {
        return new MAC();
    }

    @Override
    public Phone makePhone() {
        return new iphone();
    }
}
