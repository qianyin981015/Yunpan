package test.abstractFactory;

/**
 * @author 陈宏阳
 * @date 2020/7/13
 * Yunpan
 */
public class MAC implements PC{
    @Override
    public void make() {
    System.out.println("make MAC");
    }
    public MAC(){
        this.make();
    }
}
