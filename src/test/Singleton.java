package test;

import org.junit.Test;

/**
 * @author 陈宏阳
 * @date 2020/7/12
 * Yunpan
 */
public class Singleton {
    private Singleton(){}
    private static volatile Singleton singleton;
    public static Singleton getInstance() {
        if(singleton==null)
        {
            synchronized (Singleton.class){
                if(singleton==null)
                singleton=new Singleton();
            }
        }
        return singleton;
    }
}
