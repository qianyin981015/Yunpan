package test.simpleFactory;

/**
 * @author 陈宏阳
 * @date 2020/7/13
 * Yunpan
 */
public class factoryTest {
    public static void main(String[] args){
        phoneFactory phoneFactory=new phoneFactory();
        Phone mi=phoneFactory.make("mi");
        Phone iphone=phoneFactory.make("iphone");
    }
}
