package test.simpleFactory;

/**
 * @author 陈宏阳
 * @date 2020/7/13
 * Yunpan
 */
public class phoneFactory {
    public Phone make(String p){
        if(p=="mi"){
            return new mi();
        }
        if(p=="iphone")
        {
            return new iphone();
        }
        return null;
    }
}
