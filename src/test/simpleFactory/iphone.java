package test.simpleFactory;

/**
 * @author 陈宏阳
 * @date 2020/7/13
 * Yunpan
 */
public class iphone implements Phone{
    public iphone(){
        this.make();
    }
    @Override
    public void make() {
    System.out.println("make iphone ");
    }
}
